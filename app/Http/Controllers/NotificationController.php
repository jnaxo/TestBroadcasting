<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\EventTrigger;

class NotificationController extends Controller
{
    public function index()
    {
        return view('event-listener');
    }

    public function send()
    {
        event(new EventTrigger());
        return redirect('/');
    }
}
